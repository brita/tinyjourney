Tiny Journey is a graphical adventure where you control a tiny kid with huge dreams. With an engaging storyline, the game features text-based interaction with the world, puzzle-like challenges, and beautiful art and music.

This game was made as an entry for [Ludum Dare 23 Game Jam competition][1] under the theme "Tiny World".
We've reached the 55th place out of 330 in the overall ranking and our best position was #7 in the 'Mood' ranking. We're also achieved a place in top 50 for 'Graphics', 'Theme' and 'Audio'.

Tiny Journey has been reviewed in two German blogs: ["asamakabino"][1] and ["superlevel" (de)][3] [(en)][4]. Tiny Journey was also featured in [personal tops][5] and it's now available on [Kongregate][6].

This repository holds a series of ports of the game, starting with HTML5/JavaScript.

[1]: http://www.ludumdare.com/compo/ludum-dare-23/
[2]: http://www.asamakabino.de/52games13-einsamkeit-tiny-journey.html
[3]: http://superlevel.de/spiele/ludum-dare-23-reportage-teil-2-von-7/
[4]: http://superlevel.de/spiele/ludum-dare-23-the-report/
[5]: http://www.ludumdare.com/compo/2012/05/13/up-for-rating-a-few-more-games/
[6]: http://www.kongregate.com/games/EvilSandwichS/tiny-journey
